# Application IOS: Gestion RaspberryPi 

![](https://scontent-cdg2-1.xx.fbcdn.net/v/t34.0-12/16111595_1218204284967999_638133441_n.png?oh=7f10c734aa5fe118e9c9032c974acdaf&oe=5A510EFD)

## A quoi sert l’application ?
Grâce à RaspberryPi Remote vous pouvez contrôler les capteurs que vous avez rajoutés à votre raspberry. 

## Comment l’utiliser ?
Il suffit dans un premier temps d’enregistrer votre Raspberry via un nom et l’ip de la RaspberryPi en question dans l'application. Ensuite vous pouvez ajouter simplement les capteurs en indiquant leurs informations telles que leur type, leur broche... 
Enfin, il suffit de créer vos script grâce à notre formulaire d'algorithmie simplifiée.

![](https://scontent-cdg2-1.xx.fbcdn.net/v/t34.0-0/p480x480/16111334_1218184891636605_2074896898_n.png?oh=2d48ebad396c0ef304bb6c89e492532f&oe=5A51628C)

## Comment ajouter un capteur ?
Pour ajouter un capteur, il suffit de sélectionner le bouton "+" dans la liste des capteurs (qui est initialement vide). Vous arrivez donc sur un formulaire où vous pourrez renseigner toutes les données utile du capteur.

## Que faire en cas de bug ?
Vous pouvez nous faire part de votre problème en ouvrant un ticket sur le dépôt git à ce lien : https://gitlab.com/groups/ynov-ingesup-b3_16-17/issues

Notre application permet de définir des recettes, une instruction simplifier de code depuis l'application, sur une Raspberry Pi

--------------------------------------------------------------------------------

# Lien Cool:

-   [Data Persistence](https://code.tutsplus.com/tutorials/ios-from-scratch-with-swift-data-persistence-and-sandboxing-on-ios--cms-25505)
-   [HTTP networking library](https://github.com/Alamofire/Alamofire)

-   [open source UI components](https://www.cocoacontrols.com/)
-   [Video de ce qu'on devrait faire pour les recettes](https://www.youtube.com/watch?v=FWL8QjcOc9g)
   

Rappel: merci de commenter vos code et de bien écrire ce que fais chaque commit ! 



--------------------------------------------------------------------------------

# Page de lancement de l'application:

Cette page affiche une liste d'ip ou nom de dommaine local et internet.
Elle affichera aussi le lien du script d'installation du WebService sur GitLab (Repo publique)

- Option possible:
    - Ajouter une IP/nom de domaine et le nom de la Raspberry Pi
    - Supprimer une Raspberry Pi
    - se connecter **Pour entrer dans la partie Capteur/Recette**
- Note Importante:
    - Gestion d'erreur: si perte de connexion = redirection vers cette Page.

## une fois sur une Raspberry Pi:

### 1. Capteur

Nous pouvons rajouter des capteurs depuis notre application. 
Celui ci permet de choisir le type de capteur (Entrer/Sortie) et sa cathégorie (led, humidity sensor, potentiometer, ...). 
Une fois les informations rentrer, l'application vous donne le schemat d'installation sur le GPIO de votre Raspberry Pi.

- Option Possible:
    - la liste des capteurs: Nous donne toute les données temps réel ou 1 seconde (pour ne pas défoncer le WebService). 
    - Suppression du capteur:  Les données des capteurs ce trouve sur la raspberry pi, nous une demande du WebService est requis pour la supression
    - Création : ...

La structure d'un capteur devrait etre comme ceci:

```json
{
	"Name": "Mon_Nom_De_Capteur",
	"Description": "...",
	"TypeEntrer": true,
	"cat": "LedSimple",
	"GPIO": {
		"GND": "GND",
		"C1": 16
	},
	"typeRetour": null
}

```


### 2. Recette

Nous pouvons ajouter des recettes depuis notre application. 
Une recette est une définition précise d'étapes d'instruction que la raspberry Pi va devoir faire.

Une instruction simple est la suivante: 

1. Type condition:
    1. if : Si ...
    2. while : tant que (obsolète: les recettes serons contenu dans un while true)
2. Valeur A:
    1. Select -> Capteur : un capteur(Entrer) dans la liste
    2. Time : une dateTime
    3. Select -> Recette : une recette peux potentiellement etre vue comme un capteur(Entrer) numerique
3. comparateur:
    1. <=
    2. ==
    3. >=
    4. !=
4. Valeur B:
    1. Int/Double: via un input user
    2. dateTime: via un input user
    3. Boolean: via un input user
5. Condition a faire:
    1. Select -> Capteur: Capteur(Sortie) dans la liste
        1. Valeur<Type> : input user -> Valeur<type> attendu du type de capteur
    2. Select -> Autre Condition :
        1. Boucle Infinie de condition !
    3. Autre contition a faire: 
        1. Boucle Infini de condition a faire! 
    4. Expérimental : Return <type> -> user input (pour faire un capteur numérique)
    
    
- Option Possible:
    - Affiche:
        - Nom de la recette
        - Description
        - Liste de capteurs:
            - Nom du capteur
            - Liste GPIO:
                - Type -> GPIO_ID  (exemple : GND -> GND; C1 -> 16;)
    - Suprimer une recette 
- Idée : 
    - sauvegarder les détails des capteurs pour déployer rapidement sur une autre raspberry Pi (en affichant le détail des capteur sur le GPIO depuis la vue détail de la recette) 

