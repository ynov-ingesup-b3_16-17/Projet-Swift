//
//  NavigationVC.swift
//  RaspberryPi
//
//  Created by Nolan VIEIRA on 11/01/2017.
//  Copyright © 2017 Ludivine-David-Nolan. All rights reserved.
//

import UIKit
import Material

class NavigationVC: BottomNavigationController {
    open override func prepare() {
        super.prepare()
        prepareTabBar()
    }
    
    private func prepareTabBar() {
        tabBar.depthPreset = .none
        tabBar.dividerColor = Color.grey.lighten3
    }
}
