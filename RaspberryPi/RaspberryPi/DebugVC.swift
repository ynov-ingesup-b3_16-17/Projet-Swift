//
//  DebugVC.swift
//  RaspberryPi
//
//  Created by Nolan VIEIRA on 11/01/2017.
//  Copyright © 2017 Ludivine-David-Nolan. All rights reserved.
//

import UIKit
import Material


class DebugVC: UIViewController {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareTabBarItem()
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.red.base
    }
    
    private func prepareTabBarItem() {
        tabBarItem.title = nil
        tabBarItem.image = Icon.cm.menu?.tint(with: Color.pink.darken2)
        tabBarItem.selectedImage = Icon.cm.menu?.tint(with: Color.pink.base)
    }
}
