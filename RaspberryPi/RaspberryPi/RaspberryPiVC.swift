//
//  RaspberryPiVC.swift
//  RaspberryPi
//
//  Created by Nolan VIEIRA on 11/01/2017.
//  Copyright © 2017 Ludivine-David-Nolan. All rights reserved.
//

import UIKit
import Material

class RaspberryPiVC: UIViewController {
    fileprivate var imageCard: ImageCard!
    @IBOutlet weak var card: Card!

    var raspberryName = ""
    var raspberryIp = ""
    let dataName = UserDefaults.standard
    let dataIp = UserDefaults.standard
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareTabBarItem()
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.grey.lighten2 //Color.pink.darker1
        
        // Prepare view.
        prepareImageCard()
        
        
        
    }
    
    private func prepareTabBarItem() {
        tabBarItem.title = nil
        tabBarItem.image = Icon.cm.settings?.tint(with: Color.pink.darken2)
        tabBarItem.selectedImage = Icon.cm.settings?.tint(with: Color.pink.base)
    }
    
    
}

extension RaspberryPiVC {
    fileprivate func prepareImageCard() {
        imageCard = ImageCard()
        
        prepareToolbar()
        prepareImageView()
        prepareContentView()
        prepareBottomBar()
    }
    
    fileprivate func prepareToolbar() {
        imageCard.toolbar = Toolbar()
        imageCard.toolbar?.backgroundColor = .clear
        imageCard.toolbarEdgeInsetsPreset = .square3
        
        imageCard.toolbar?.title = "Graph"
        imageCard.toolbar?.titleLabel.textColor = .white
        
        imageCard.toolbar?.detail = "Build Data-Driven Software"
        imageCard.toolbar?.detailLabel.textColor = .white
    }
    
    fileprivate func prepareImageView() {
        imageCard.imageView = UIImageView()
        imageCard.imageView?.image = UIImage(named: "raspberrypi.png")?.resize(toWidth: view.width)
    }
    
    fileprivate func prepareContentView() {
        let label = UILabel()
        label.numberOfLines = 0
        if dataName.value(forKey: "Name") != nil {
            raspberryName = dataName.value(forKey: "Name") as! String
            label.text = "votre \(raspberryName) est visible"
        }
        else {
            label.text = "Pas de Raspberry enregistré"
        }
        label.font = RobotoFont.regular(with: 14)
        label.textAlignment = .center
        
        imageCard.contentView = label
        imageCard.contentViewEdgeInsetsPreset = .square3
    }
    
    fileprivate func prepareBottomBar() {
        let addRaspberry = IconButton(image: Icon.cm.edit, tintColor: Color.blueGrey.base)
        let statusRaspberry = IconButton(image: Icon.visibility, tintColor: Color.red.base)
        
        addRaspberry.addTarget(self, action: #selector(redirectAddRaspberry), for: .touchUpInside)
        
        let label = UILabel()
        
        if dataIp.value(forKey: "ip") != nil {
            raspberryIp = dataIp.value(forKey: "ip") as! String
            label.text = "\(raspberryIp)"
        }
        else {
            label.text = "Aucune IP ou Nom de domaine enregistré."
        }

        label.textAlignment = .center
        label.textColor = Color.blueGrey.base
        label.font = RobotoFont.regular(with: 12)
        
        imageCard.bottomBar = Bar()
        imageCard.bottomBarEdgeInsetsPreset = .wideRectangle2
        imageCard.bottomBarEdgeInsets.top = 0
        
        imageCard.bottomBar?.leftViews = [statusRaspberry]
        imageCard.bottomBar?.centerViews = [label]
        imageCard.bottomBar?.rightViews = [addRaspberry]
        
        view.layout(imageCard).horizontally(left: 20, right: 20).center()
    }
    
    func redirectAddRaspberry(){
        let vc = AddRaspberryVC()
        self.present(vc, animated: true, completion: nil)
        
    }
}
