//
//  CapteurVC.swift
//  RaspberryPi
//
//  Created by Nolan VIEIRA on 11/01/2017.
//  Copyright © 2017 Ludivine-David-Nolan. All rights reserved.
//

import UIKit
import Material

class CapteurVC: UIViewController {
    
    open var dataSourceItems = [DataSourceItem]()
    
    fileprivate var toolbar: Toolbar!
    fileprivate var tableView: TableView!
    fileprivate var card: Card!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareTabBarItem()
        
        
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.grey.lighten2
        
        prepareToolbar()
        prepareTableView()
        prepareCard()
        prepareData()
    }
    
    private func prepareTabBarItem() {
        tabBarItem.title = nil
        tabBarItem.image = Icon.cm.shuffle?.tint(with: Color.pink.darken2)
        tabBarItem.selectedImage = Icon.cm.shuffle?.tint(with: Color.pink.base)
    }
}

extension CapteurVC {
    fileprivate func prepareToolbar() {
        toolbar = Toolbar()
        toolbar.title = "Liste de capteurs"
        toolbar.detail = "en cours de création..."
    }
    
    fileprivate func prepareTableView() {
        tableView = TableView()
        tableView.height = 500
        //tableView.position = CGPoint(x: 0,y: 0)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TableViewCell.self, forCellReuseIdentifier: "TableViewCell")
    }
    
    fileprivate func prepareCard() {
        card = Card()
        card.toolbar = toolbar
        card.contentView = tableView
        view.layout(card).horizontally(left: 20, right: 20).center()
    }
    
    fileprivate func prepareData() {
        let RaspberryPis = [["name": "LedRouge"], ["name": "LedVerte"]]
        for RaspberryPi in RaspberryPis {
            dataSourceItems.append(DataSourceItem(data: RaspberryPi))
        }
        tableView.reloadData()
    }
}

extension CapteurVC: TableViewDelegate {}

extension CapteurVC: TableViewDataSource {
    @objc
    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @objc
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceItems.count
    }
    
    @objc
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        guard let data = dataSourceItems[indexPath.row].data as? [String: String] else {
            return cell
        }
   

        cell.textLabel?.text = data["name"]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {

        //print(dataSourceItems[indexPath.row])
        
    }
}

