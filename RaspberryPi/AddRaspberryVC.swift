//
//  AddRaspberryVC.swift
//  
//
//  Created by David Braz on 12/01/2017.
//
//

import UIKit
import Material

class AddRaspberryVC: UIViewController {
    private var raspberryPiField: TextField!
    private var ipOrHostNameField: TextField!
    private let constant: CGFloat = 32
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.grey.lighten2 //Color.pink.darker1
        
        prepareNameField()
        prepareIpField()
        prepareResignResponderButton()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Programmatic update for the textField as it rotates.
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        raspberryPiField.width = view.height - 2 * constant
    }
    
    /// Prepares the resign responder button.
    fileprivate func prepareResignResponderButton() {
        let btn = RaisedButton(title: "Validation", titleColor: Color.pink.darken2)
        btn.addTarget(self, action: #selector(redirectToRaspberryPiVC), for: .touchUpInside)
        
        view.layout(btn).width(100).height(constant).top(24).right(24)
    }

   func redirectToRaspberryPiVC() {
        //raspberryPiField?.resignFirstResponder()
       //ipOrHostNameField?.resignFirstResponder()
        UserDefaults.standard.set(raspberryPiField.text!, forKey: "Name" )
        UserDefaults.standard.synchronize()
        UserDefaults.standard.set(ipOrHostNameField.text!, forKey: "ip")
        UserDefaults.standard.synchronize()
        dismiss(animated: true, completion: nil)
    }
    
    private func prepareNameField() {
        raspberryPiField = TextField()
        raspberryPiField.placeholder = "Nom de votre RaspberryPi"
        raspberryPiField.detail = "Identification de votre RaspberryPi"
        raspberryPiField.isClearIconButtonEnabled = true
        if UserDefaults.standard.value(forKeyPath: "Name") != nil {
            raspberryPiField.text = UserDefaults.standard.value(forKeyPath: "Name") as! String
        }
        
        let leftView = UIImageView()
        leftView.image = Icon.home
        
        raspberryPiField.leftView = leftView
        raspberryPiField.leftViewMode = .always
        raspberryPiField.leftViewNormalColor = Color.pink.darken2
        raspberryPiField.leftViewActiveColor = Color.pink.base
        
        view.layout(raspberryPiField).top(4 * constant).horizontally(left: constant, right: constant)
    }
    
    private func prepareIpField() {
        ipOrHostNameField = TextField()
        ipOrHostNameField.placeholder = "192.168.1.X ou un nom de domaine"
        ipOrHostNameField.detail = "Identification reseau de votre RaspberryPi"
        ipOrHostNameField.isClearIconButtonEnabled = true
        if UserDefaults.standard.value(forKeyPath: "ip") != nil {
            ipOrHostNameField.text =  UserDefaults.standard.value(forKeyPath: "ip") as! String
        }

        
        let leftView = UIImageView()
        leftView.image = Icon.share
        
        ipOrHostNameField.leftView = leftView
        ipOrHostNameField.leftViewMode = .always
        ipOrHostNameField.leftViewNormalColor = Color.pink.darken2
        ipOrHostNameField.leftViewActiveColor = Color.pink.base
        
        view.layout(ipOrHostNameField).top(7 * constant).horizontally(left: constant, right: constant)
        
    }

    
}

extension AddRaspberryVC : TextFieldDelegate {
    /// Executed when the 'return' key is pressed when using the emailField.
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = true
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    public func textField(textField: UITextField, didChange text: String?) {
        print("did change", text ?? "")
    }
    
    public func textField(textField: UITextField, willClear text: String?) {
        print("will clear", text ?? "")
    }
    
    public func textField(textField: UITextField, didClear text: String?) {
        print("did clear", text ?? "")
    }
}
